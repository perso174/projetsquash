Feature: JPet-Connexion

	Scenario Outline: JPet-Connexion
		Given un navigateur est ouvert
		When je suis sur url
		And je clique sur le lien de connexion
		And rentre le Username <user>
		And rentre le Password <pwd>
		And je clique sur login
		Then utilisateur ABC est connecte
		And je peux lire le message accueil <message>

		@AdminProfil
		Examples:
		| message | pwd | user |
		| "Welcome ABC!" | "j2ee" | "j2ee" |

		@UserProfil
		Examples:
		| message | pwd | user |
		| "Welcome ABC!" | "ACID" | "ACID" |


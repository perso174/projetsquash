package gherkinJava;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class JPetStroreSteps{
	
//	WebDriver driver;
	
	@Given("un navigateur est ouvert")
	public void un_navigateur_est_ouvert() {
		System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
		DriverHandler.setDriver(new ChromeDriver());
		DriverHandler.getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@When("je suis sur url")
	public void je_suis_sur_url() {
		DriverHandler.getDriver().get("https://petstore.octoperf.com/actions/Catalog.action");
	}

	@When("je clique sur le lien de connexion")
	public void je_clique_sur_le_lien_de_connexion() {
		DriverHandler.getDriver().findElement(By.xpath("//*[@id='MenuContent']/a[2]")).click();
	}

	@When("rentre le Username {string}")
	public void rentre_le_Username(String username) {
		DriverHandler.getDriver().findElement(By.xpath("//*[@name='username']")).clear();
		DriverHandler.getDriver().findElement(By.xpath("//*[@name='username']")).sendKeys(username);
	}

	@When("rentre le Password {string}")
	public void rentre_le_Password(String password) {
		DriverHandler.getDriver().findElement(By.xpath("//*[@name='password']")).clear();
		DriverHandler.getDriver().findElement(By.xpath("//*[@name='password']")).sendKeys(password);
	}

	@When("je clique sur login")
	public void je_clique_sur_login() {
		DriverHandler.getDriver().findElement(By.xpath("//*[@name='signon']")).click();

	}

	@Then("utilisateur ABC est connecte")
	public void utilisateur_ABC_est_connecte() {
		boolean imageShown = DriverHandler.getDriver().findElement(By.id("MainImageContent")).isDisplayed();
		assertEquals(true, imageShown);
	}

	@Then("je peux lire le message accueil {string}")
	public void je_peux_lire_le_message_accueil(String message) {
		String messageCapt = DriverHandler.getDriver().findElement(By.id("WelcomeContent")).getText();
		assertEquals(message, messageCapt);
	}
	
}

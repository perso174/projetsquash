package gherkinJava;

import io.cucumber.java.After;

public class Setup {

	@After
	public void teardown() {
		DriverHandler.getDriver().quit();
	}
}

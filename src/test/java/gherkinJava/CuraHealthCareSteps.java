package gherkinJava;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CuraHealthCareSteps {
//	WebDriver driver;
	
	String login;
	String pwd;
	
	String facility;
	String admission;
	String programS;
	String date;
	String comment;


	
	@Given("Je suis connecte sur la page de prise de rendez-vous")
	public void je_suis_connect_sur_la_page_de_prise_de_rendez_vous(List<Map<String, String>> entry) {
		System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
		DriverHandler.setDriver(new ChromeDriver());
	    DriverHandler.getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    DriverHandler.getDriver().get("https://katalon-demo-cura.herokuapp.com/");
	    
	    for (Map<String, String> map : entry) {
			login=map.get("login");
			pwd=map.get("pwd");
		}
	    
	    DriverHandler.getDriver().findElement(By.id("btn-make-appointment")).click();
	    
	    DriverHandler.getDriver().findElement(By.id("txt-username")).clear();
	    DriverHandler.getDriver().findElement(By.id("txt-username")).sendKeys(login);

	    DriverHandler.getDriver().findElement(By.id("txt-password")).clear();
	    DriverHandler.getDriver().findElement(By.id("txt-password")).sendKeys(pwd);
	    
	    DriverHandler.getDriver().findElement(By.id("btn-login")).click();

	}

	@When("Je renseigne les informations obligatoires")
	public void je_renseigne_les_informations_obligatoires(List<Map<String, String>> entry) {
	    for (Map<String, String> map : entry) {
			facility=map.get("facility");
			programS=map.get("program");
			admission=map.get("admission");
			date=map.get("date");
			comment=map.get("comment");

		}
		
	    Select facilitySe = new Select(DriverHandler.getDriver().findElement(By.id("combo_facility")));
	    facilitySe.selectByVisibleText(facility);
	    
	    if(admission.equals("Yes")) {
	    	DriverHandler.getDriver().findElement(By.id("chk_hospotal_readmission")).click();
	    }
	    
	    List<WebElement> program = DriverHandler.getDriver().findElements(By.name("programs"));
	    for (int i = 0; i < program.size(); i++) {
	    	String value = program.get(i).getAttribute("value");
			if(value.trim().equals(programS)) {
				program.get(i).click();
				break;
			}
		}
	    
	    DriverHandler.getDriver().findElement(By.id("txt_visit_date")).sendKeys(date);

	    DriverHandler.getDriver().findElement(By.id("txt_comment")).sendKeys(comment);
	    
	}

	@When("Je clique sur Book Appointment")
	public void je_clique_sur_Book_Appointment() throws InterruptedException {
		DriverHandler.getDriver().findElement(By.id("btn-book-appointment")).click();

	}

	@Then("Le rendez-vous est confirme")
	public void le_rendez_vous_est_confirm() {
		assertEquals(facility, DriverHandler.getDriver().findElement(By.id("facility")).getText());
		assertEquals(admission, DriverHandler.getDriver().findElement(By.id("hospital_readmission")).getText());
		assertEquals(programS, DriverHandler.getDriver().findElement(By.id("program")).getText());
		assertEquals(date, DriverHandler.getDriver().findElement(By.id("visit_date")).getText());
		assertEquals(comment, DriverHandler.getDriver().findElement(By.id("comment")).getText());

		assertTrue(DriverHandler.getDriver().findElement(By.xpath("//a[.='Go to Homepage']")).isDisplayed());
		
	}
}

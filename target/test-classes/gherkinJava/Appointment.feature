# language: en
Feature: Prise de rendez vous

	Scenario: Prise de rendez vous
		Given Je suis connecte sur la page de prise de rendez-vous
			| login| pwd|
			|John Doe|ThisIsNotAPassword|
		When Je renseigne les informations obligatoires
			| facility|admission|program|date|comment|
			|Seoul CURA Healthcare Center|Yes|None|30/06/2022|xyz|
		And Je clique sur Book Appointment
		Then Le rendez-vous est confirme
Feature: JPet

  Scenario: Connexion
    Given un navigateur est ouvert
    When je suis sur url
    And je clique sur le lien de connexion
    And rentre le Username "j2ee"
    And rentre le Password "j2ee"
    And je clique sur login
    Then utilisateur ABC est connecte
    And je peux lire le message accueil "Welcome ABC!"
